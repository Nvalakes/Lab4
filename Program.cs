﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lab4
{

    class Currency
    {
        public string Name;
        public double Low; // нижняя цена, цена продажи 
        public double Hight;  // верхняя цена, цена покупки
    }

    class Program
    {
        static List<Currency> LoadData(string file)
        {
            var result = new List<Currency>();

            using (StreamReader sr = File.OpenText(file))
            {
                sr.ReadLine(); // пропускаю первую строку
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    string[] parts = line.Split();

                    Currency c = new Currency();
                    c.Name = parts[0];
                    c.Low = Convert.ToDouble(parts[1]);
                    c.Hight = Convert.ToDouble(parts[2]);
                    result.Add(c);
                }
            }

            return result;
        }

        static void CreateFile(List<Currency> data, Currency currency)
        {
            string fileName = currency.Name + ".txt";
            using (StreamWriter sw = File.CreateText(fileName))
            {
                sw.WriteLine(currency.Name);
                foreach (Currency c1 in data)
                {
                    if (c1 == currency)
                    {
                        continue;
                    }

                    sw.Write(c1.Name + " ");

                    // необходимо узнать, сколько можно купить/продать валюты c1 за валюту currency                     
                    double count = c1.Low / currency.Hight;
                    sw.Write(string.Format("{0:N4} ", count));

                    // ...                    
                    count = c1.Hight / currency.Low;
                    sw.WriteLine(string.Format("{0:N4}", count));
                }                                    
            }
        }

        static void Main(string[] args)
        {
            List<Currency> data = LoadData("RUB.txt");
            data.Insert(0, new Currency { Name = "RUB", Low = 1, Hight = 1 });
            foreach (Currency currency in data.Skip(1))  // пропуская рубль (он первый в списке)
            {
                CreateFile(data, currency);
            }

        }
    }
}
